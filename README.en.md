# Itcast GenkiPi

- [Introduction](#section1)
- [Repository](#section2)


## Introduction<a name="section1"></a>

GenkiPi development board is produced by Itcast  Co., Ltd. It is equipped with OpenHarmony2.0 operating system; based on Hisilicon HI3861 module, 160MHz main frequency, SRAM 352KB, ROM 288KB, 2M Flash. 。It supports development protocols such as SPI, IIC, UART, ADC, and PWM, and is widely used in smart wear, smart security and industrial IoT scenarios.

![](./asserts/board.png)

|  引脚   |      Uart       |  SPI  |  ADC  |  PWM  |  I2S  | SDIO |  I2C  |
| :-----: | :-------------: | :---: | :---: | :---: | :---: | :--: | :---: |
| GPIO_07 |      CTS_1      | RXD_0 | ADC_3 | PWM_0 | CLK_0 |      |       |
| GPIO_08 |      RTS_1      | TXD_0 |       | PWM_1 | WS_0  |      |       |
| GPIO_10 |      CTS_2      | CLK_0 |       | PWM_1 | TX_0  |  D3  | SDA_0 |
| GPIO_09 |      RTS_2      | TXD_0 | ADC_4 | PWM_0 | MCK_0 |  D2  | SCL_0 |
| GPIO_03 |    LOG_TXD_0    |       |       |       |       |      |       |
| GPIO_04 |    LOG_RXD_0    |       | ADC_1 |       |       |      |       |
| GPIO_02 |                 |       |       | PWM_2 | MCK_0 |      |       |
| GPIO_05 |      RXD_1      | CSI_0 | ADC_2 | PWM_2 | TX_0  |      |       |
| GPIO_06 |      TXD_1      | CLK_0 |       | PWM_3 |       |      |       |
| GPIO_14 | LOG_RXD_0/CTS_2 |       |       | PWM_5 | RX_0  |  D1  | SCL_0 |
| GPIO_11 |      TXD_2      | RXD_0 | ADC_5 | PWM_2 | CLK_0 | CMD  |       |
| GPIO_12 |      RXD_2      | CSI_0 | ADC_0 | PWM_3 | WS_0  | CLK  |       |
| GPIO_13 | LOG_TXD_0/RTS_2 |       | ADC_6 | PWM_4 |       |  D0  | SDA_0 |





## Repository<a name="section2"></a>
[device/itcast/genkipi](https://gitee.com/openharmony-sig/devboard_device_itcast_genkipi)




